import { CanActivate, createParamDecorator, ExecutionContext, Injectable, SetMetadata } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import * as Jwt from 'jsonwebtoken';
import { User } from 'src/users/domain/entities';

export const IS_PUBLIC_KEY = 'isPublic';

export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);

export const CurrUser = createParamDecorator((key: keyof User, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest<Request>();
  const user = request['user'];

  return key ? user[key] : user;
});

@Injectable()
export class LoginGuard implements CanActivate {
  public constructor(private readonly reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<Request>();
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) return true;

    const accessTokenPayload = await this.verifyAccessToken(req);
    if (!accessTokenPayload) return false;

    req['user'] = accessTokenPayload;
    return true;
  }

  private async verifyAccessToken(req: Request) {
    if (!req.headers.authorization) return false;

    const accessToken = req.headers.authorization.split('Bearer ')[1];
    if (!accessToken) return false;

    return Jwt.verify(accessToken, process.env.JWT_SECRET);
  }
}
