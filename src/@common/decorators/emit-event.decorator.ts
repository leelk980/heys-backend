import { IEvent } from '@nestjs/cqrs';
import { BaseAggregate, BaseEntity } from '../bases';

export const EmitEvent = <TEntity extends BaseEntity, TResult = any>(event: new (...args: any[]) => IEvent) => {
  return function (
    _target: any,
    _name: string,
    descriptor: TypedPropertyDescriptor<(...args: any[]) => Promise<TResult>>,
  ): TypedPropertyDescriptor<(...args: any[]) => Promise<TResult>> {
    const originalMethod = descriptor.value;

    return {
      configurable: true,
      enumerable: false,
      get() {
        return async (data: any, options: any = null) => {
          const that = this as BaseAggregate<TEntity>;
          const result = await originalMethod.call(that, data, options);

          that.apply(new event(data, options, result));

          return result as TResult;
        };
      },
    };
  };
};
