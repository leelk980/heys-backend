import { BaseCommand, BaseCommandHandler } from '../bases';

export const Transaction = <TCommand extends BaseCommand<any>>() => {
  return function (
    _target: any,
    _name: string,
    descriptor: TypedPropertyDescriptor<BaseCommandHandler<TCommand>['execute']>,
  ): TypedPropertyDescriptor<BaseCommandHandler<TCommand>['execute']> {
    const originalMethod = descriptor.value;

    return {
      configurable: true,
      enumerable: false,
      get() {
        return async (command: TCommand) => {
          const that = this as BaseCommandHandler<TCommand>;
          const queryRunner = await that.createQueryRunner();
          const aggregates = await that.createAggregatesByQueryRunner(queryRunner);

          let result: TCommand['result'];
          let error: Error;
          try {
            result = await originalMethod.call(that, command, aggregates);
            for (const aggregate of aggregates) {
              aggregate.commit();
            }

            await queryRunner.commitTransaction();
          } catch (err) {
            error = err;
            await queryRunner.rollbackTransaction();
          } finally {
            await queryRunner.release();
          }

          if (error) throw error;
          return result;
        };
      },
    };
  };
};
