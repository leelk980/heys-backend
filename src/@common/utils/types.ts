import { AggregateRoot } from '@nestjs/cqrs';
import { BaseAggregate, BaseEntity } from '../bases';

export type NonEmptyArray<T> = [T, ...T[]];

export type Aggregates<
  T extends BaseAggregate<any>,
  U extends BaseAggregate<any> = null,
  V extends BaseAggregate<any> = null,
  W extends BaseAggregate<any> = null,
  X extends BaseAggregate<any> = null,
> = [
  Omit<T, keyof AggregateRoot<any> | 'setQueryRunner'>,
  Omit<U, keyof AggregateRoot<any> | 'setQueryRunner'>,
  Omit<V, keyof AggregateRoot<any> | 'setQueryRunner'>,
  Omit<W, keyof AggregateRoot<any> | 'setQueryRunner'>,
  Omit<X, keyof AggregateRoot<any> | 'setQueryRunner'>,
];

export type CreateInput<T extends BaseEntity, K extends keyof Omit<T, keyof BaseEntity> = null> = Omit<
  T,
  keyof BaseEntity | K
> &
  Partial<{ [key in K]: T[K] }>;

export type UpdateInput<T extends BaseEntity> = Partial<Omit<T, keyof BaseEntity>>;
