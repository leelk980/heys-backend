import { EventPublisher } from '@nestjs/cqrs';
import { Connection, QueryRunner } from 'typeorm';
import { BaseAggregate } from './base.aggregate';
import { BaseCommand } from './base.command';
import { Aggregates, NonEmptyArray } from '../utils';

/** BaseCommandHandler
 * Property
 * - publisher: aggregate의 메소드에서 event를 publish하기 위해 주입받는다
 * - connection: 트랜잭션 관리를 위해 typeorm 주입받는다
 * - aggregates: domain의 aggregate들을 주입 받고, aggregate들이 비즈니스 로직을 수행한다
 *
 * Method
 * - constructor: 핸들러에서 사용할 aggregates를 등록한다
 * - mergeObjectContext: 주입받은 aggregate들이 event를 publish하기 위한 능력을 부여하는 작업 (nestjs cqrs 컨벤션)
 *    => TODO: 생성자에서 super(...) 다음에 반드시 실행해야하는데, 제약을 거는 방법을 모르겠음
 * - createQueryRunner: 트랜잭션을 실행시킬 queryRunner를 받아온다
 * - createAggregatesByQueryRunner: 핸들러의 하위 queryRunner를 주입시킨 aggregate들을 생성
 */

export abstract class BaseCommandHandler<TCommand extends BaseCommand<any>> {
  protected abstract publisher: EventPublisher;
  protected abstract connection: Connection;
  protected readonly aggregates: NonEmptyArray<BaseAggregate<any>>;

  protected constructor(...aggregates: NonEmptyArray<BaseAggregate<any>>) {
    this.aggregates = aggregates;
  }

  abstract execute(command: TCommand, aggregates: Aggregates<BaseAggregate<any>>): Promise<TCommand['result']>;

  public async createQueryRunner() {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    return queryRunner;
  }

  public async createAggregatesByQueryRunner(queryRunner: QueryRunner) {
    return this.aggregates.map((aggregate) => {
      const clone = Object.create(aggregate) as BaseAggregate<any>;
      clone.setQueryRunner(queryRunner);
      return this.publisher.mergeObjectContext(clone);
    });
  }
}
