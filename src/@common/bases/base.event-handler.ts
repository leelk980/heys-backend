import { BaseEvent } from './base.event';

export abstract class BaseEventHandler<TBaseEvent extends BaseEvent<any>> {
  abstract handle(event: TBaseEvent): any;
}
