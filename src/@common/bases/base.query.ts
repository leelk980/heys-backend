export abstract class BaseQuery<TResult> {
  public readonly result: TResult;
}
