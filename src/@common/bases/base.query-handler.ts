import { BaseQuery } from './base.query';

export abstract class BaseQueryHandler<TQuery extends BaseQuery<any>> {
  abstract execute(command: TQuery): Promise<TQuery['result']>;
}
