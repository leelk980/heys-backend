import { AggregateRoot } from '@nestjs/cqrs';
import { QueryRunner, Repository } from 'typeorm';
import { BaseEntity } from './base.entity';
import { NonEmptyArray } from '../utils';

/** BaseAggregate
 * Property
 * - root: 로직을 수행하고자하는 root record
 *    => setter를 만든이유: 밖에서 get은 못하지만 밖에서 주입해주는거라서
 * - queryRunner: application 레이어(CommandHanlder)에서 주입받은 queryRunner
 *    => why? 서로 다른 aggregate간의 트랜잭션을 유지하기 위함
 * - base-repositories: Aggregate에서 사용하는 repository 목록
 *    => ** rootRepository를 첫번째에 등록해야함!
 *
 * Method
 * - setRootById: 핸들러에서 조작할 root링Entity 설정할 때 사용
 * - setQueryRunner: 핸들러의 @Transcation 데코레이터에서 queryRunner 주입할 때 사용
 */
export abstract class BaseAggregate<TRootEntity extends BaseEntity> extends AggregateRoot {
  protected root?: TRootEntity;
  protected queryRunner: QueryRunner;
  private readonly repositories: NonEmptyArray<Repository<any>>;

  protected constructor(...repositories: NonEmptyArray<Repository<any>>) {
    super();
    this.repositories = repositories;
  }

  public async setRootById(rootId: number) {
    if (this.root?.id === rootId) return;
    this.root = await this.repositories[0].findOne(rootId); // OrFail && rootRepository는 첫번째에 등록해야한다
  }

  public async setQueryRunner(queryRunner: QueryRunner) {
    this.queryRunner = queryRunner;
    for (let repository of this.repositories) {
      repository = this.queryRunner.manager.getRepository(repository.metadata.name);
    }
  }
}
