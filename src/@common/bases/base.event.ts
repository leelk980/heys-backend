export abstract class BaseEvent<TMethod extends (...args: any[]) => any> {
  public constructor(
    public readonly data: Parameters<TMethod>[0],
    public readonly options: Parameters<TMethod>[1],
    public readonly result: ReturnType<TMethod>,
  ) {}
}
