export abstract class BaseCommand<TResult> {
  public readonly result: TResult;
}
