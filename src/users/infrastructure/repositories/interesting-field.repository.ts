import { BaseInterestingFieldRepository } from '../../domain/base-repositories';
import { EntityRepository } from 'typeorm';
import { InterestingField } from '../../domain/entities';

@EntityRepository(InterestingField)
export class InterestingFieldRepository extends BaseInterestingFieldRepository {}
