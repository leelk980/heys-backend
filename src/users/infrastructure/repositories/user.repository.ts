import { User } from '../../domain/entities';
import { BaseUserRepository } from '../../domain/base-repositories';
import { EntityRepository } from 'typeorm';

@EntityRepository(User)
export class UserRepository extends BaseUserRepository {
  public async findAllByUserName(): Promise<User> {
    return undefined;
  }
}
