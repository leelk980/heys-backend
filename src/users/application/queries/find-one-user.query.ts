import { BaseQuery } from 'src/@common/bases';
import { FindOneUserView } from 'src/users/interface/views';

export class FindOneUserQuery extends BaseQuery<FindOneUserView> {
  public constructor(public readonly id: number) {
    super();
  }
}
