import { BaseQuery } from '../../../@common/bases';
import { FindAllInterestingFieldView } from '../../interface/views';

export class FindAllInterestingFieldQuery extends BaseQuery<FindAllInterestingFieldView> {
  public constructor(public readonly queryString: any) {
    super();
  }
}
