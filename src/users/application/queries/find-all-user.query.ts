import { BaseQuery } from 'src/@common/bases';
import { FindAllUserView } from 'src/users/interface/views';

export class FindAllUserQuery extends BaseQuery<FindAllUserView> {
  public constructor(public readonly queryString: any) {
    super();
  }
}
