import { QueryHandler } from '@nestjs/cqrs';
import { BaseQueryHandler } from 'src/@common/bases';
import { UserRepository } from 'src/users/infrastructure/repositories';
import { FindUserWithPhoneNumberQuery } from './index';
import { CheckUserWithPhoneNumberView } from '../../interface/views';

@QueryHandler(FindUserWithPhoneNumberQuery)
export class FindUserWithPhoneNumberHandler extends BaseQueryHandler<FindUserWithPhoneNumberQuery> {
  public constructor(private readonly userRepository: UserRepository) {
    super();
  }

  async execute(query: FindUserWithPhoneNumberQuery) {
    const { phoneNumber } = query;
    const user = await this.userRepository.findOne({ phoneNumber: phoneNumber });

    return !user ? new CheckUserWithPhoneNumberView(false) : new CheckUserWithPhoneNumberView(true);
  }
}
