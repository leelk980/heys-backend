import { BaseQuery } from 'src/@common/bases';
import { CheckUserWithPhoneNumberView } from '../../interface/views';

export class FindUserWithPhoneNumberQuery extends BaseQuery<CheckUserWithPhoneNumberView> {
  public constructor(public readonly phoneNumber: string) {
    super();
  }
}
