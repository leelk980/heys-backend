import { QueryHandler } from '@nestjs/cqrs';
import { BaseQueryHandler } from 'src/@common/bases';
import { FindAllInterestingFieldQuery } from './index';
import { FindAllInterestingFieldView, InterestingFieldView } from '../../interface/views';
import { InterestingFieldRepository } from '../../infrastructure/repositories';

@QueryHandler(FindAllInterestingFieldQuery)
export class FindAllInterestingFieldHandler extends BaseQueryHandler<FindAllInterestingFieldQuery> {
  public constructor(private readonly interestingFieldRepository: InterestingFieldRepository) {
    super();
  }

  async execute() {
    const interestingFieldViewList = [];
    this.interestingFieldRepository.find().then((interestingFieldList) =>
      interestingFieldList.forEach(function (interestingField) {
        interestingFieldViewList.push(new InterestingFieldView(interestingField.id, interestingField.name));
      }),
    );
    return new FindAllInterestingFieldView(interestingFieldViewList);
  }
}
