import { QueryHandler } from '@nestjs/cqrs';
import { BaseQueryHandler } from 'src/@common/bases';
import { UserRepository } from 'src/users/infrastructure/repositories';
import { FindOneUserQuery } from './index';

@QueryHandler(FindOneUserQuery)
export class FindOneUserHandler extends BaseQueryHandler<FindOneUserQuery> {
  public constructor(private readonly userRepository: UserRepository) {
    super();
  }

  async execute(query: FindOneUserQuery) {
    return this.userRepository.findOne(query.id);
  }
}
