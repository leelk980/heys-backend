import { QueryHandler } from '@nestjs/cqrs';
import { BaseQueryHandler } from 'src/@common/bases';
import { UserRepository } from 'src/users/infrastructure/repositories';
import { FindAllUserQuery } from './index';

@QueryHandler(FindAllUserQuery)
export class FindAllUserHandler extends BaseQueryHandler<FindAllUserQuery> {
  public constructor(private readonly userRepository: UserRepository) {
    super();
  }

  async execute(query: FindAllUserQuery) {
    return this.userRepository.find({ where: query });
  }
}
