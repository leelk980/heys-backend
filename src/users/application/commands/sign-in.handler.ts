import { CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { BaseCommandHandler } from 'src/@common/bases';
import { Transaction } from 'src/@common/decorators';
import { Aggregates } from 'src/@common/utils';
import { UserAggregate } from 'src/users/domain';
import { Connection } from 'typeorm';
import { SignInCommand } from './index';

@CommandHandler(SignInCommand)
export class SignInHandler extends BaseCommandHandler<SignInCommand> {
  public constructor(
    protected readonly publisher: EventPublisher,
    protected readonly connection: Connection,
    protected readonly userAggregate: UserAggregate,
  ) {
    super(userAggregate);
  }

  @Transaction()
  async execute(command: SignInCommand, aggregates: Aggregates<UserAggregate>) {
    const { data } = command;
    const [userAggregate] = aggregates;
    const accessToken = await userAggregate.signIn(data);
    return { accessToken };
  }
}
