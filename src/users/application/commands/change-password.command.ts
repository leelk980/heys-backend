import { BaseCommand } from 'src/@common/bases';
import { ChangePasswordDto } from 'src/users/interface/dtos';

export class ChangePasswordCommand extends BaseCommand<void> {
  constructor(public readonly userId: number, public readonly data: ChangePasswordDto) {
    super();
  }
}
