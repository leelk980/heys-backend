import { CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { BaseCommandHandler } from 'src/@common/bases';
import { Transaction } from 'src/@common/decorators';
import { Aggregates } from 'src/@common/utils';
import { UserAggregate } from 'src/users/domain/user.aggregate';
import { Connection } from 'typeorm';
import { UpdateUserCommand } from './index';

@CommandHandler(UpdateUserCommand)
export class UpdateUserHandler extends BaseCommandHandler<UpdateUserCommand> {
  public constructor(
    protected readonly publisher: EventPublisher,
    protected readonly connection: Connection,
    protected readonly userAggregate: UserAggregate,
  ) {
    super(userAggregate);
  }

  @Transaction()
  async execute(command: UpdateUserCommand, aggreagtes: Aggregates<UserAggregate>) {
    const { userId, data } = command;
    const [userAggregate] = aggreagtes;
    await userAggregate.setRootById(userId);
    await userAggregate.update(data);

    return 1 as any;
  }
}
