import { BaseCommand } from 'src/@common/bases';
import { CreateUserDto } from 'src/users/interface/dtos';
import { CreateUserView } from 'src/users/interface/views';

export class CreateUserCommand extends BaseCommand<CreateUserView> {
  constructor(public readonly data: CreateUserDto) {
    super();
  }
}
