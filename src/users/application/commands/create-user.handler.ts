import { CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { BaseCommandHandler } from 'src/@common/bases';
import { Transaction } from 'src/@common/decorators';
import { Aggregates } from 'src/@common/utils';
import { UserAggregate } from 'src/users/domain';
import { Connection } from 'typeorm';
import { CreateUserCommand } from './index';

@CommandHandler(CreateUserCommand)
export class CreateUserHandler extends BaseCommandHandler<CreateUserCommand> {
  public constructor(
    protected readonly publisher: EventPublisher,
    protected readonly connection: Connection,
    protected readonly userAggregate: UserAggregate,
  ) {
    super(userAggregate);
  }

  @Transaction()
  async execute(command: CreateUserCommand, aggregates: Aggregates<UserAggregate>) {
    const { data } = command;
    const [userAggregate] = aggregates;
    const accessToken = await userAggregate.create(data);
    return { accessToken };
  }
}
