import { BaseCommand } from 'src/@common/bases';
import { UpdateUserDto } from 'src/users/interface/dtos';
import { UpdateUserView } from 'src/users/interface/views';

export class UpdateUserCommand extends BaseCommand<UpdateUserView> {
  constructor(public readonly userId: number, public readonly data: UpdateUserDto) {
    super();
  }
}
