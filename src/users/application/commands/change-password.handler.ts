import { CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { BaseCommandHandler } from 'src/@common/bases';
import { Transaction } from 'src/@common/decorators';
import { Aggregates } from 'src/@common/utils';
import { UserAggregate } from 'src/users/domain';
import { Connection } from 'typeorm';
import { ChangePasswordCommand } from './index';

@CommandHandler(ChangePasswordCommand)
export class ChangePasswordHandler extends BaseCommandHandler<ChangePasswordCommand> {
  public constructor(
    protected readonly publisher: EventPublisher,
    protected readonly connection: Connection,
    protected readonly userAggregate: UserAggregate,
  ) {
    super(userAggregate);
  }

  @Transaction()
  async execute(command: ChangePasswordCommand, aggregates: Aggregates<UserAggregate>) {
    const { userId, data } = command;
    const [userAggregate] = aggregates;

    await userAggregate.changePassword({
      id: userId,
      password: data.password,
    });
  }
}
