import { BaseCommand } from 'src/@common/bases';
import { SignInDto } from 'src/users/interface/dtos';
import { SignInView } from 'src/users/interface/views';

export class SignInCommand extends BaseCommand<SignInView> {
  public constructor(public readonly data: SignInDto) {
    super();
  }
}
