import { UserGenderEnum } from 'src/@common/enums';
import { getConnection } from 'typeorm';
import { UsersController } from '../interface/users.controller';

describe('USER: Create', () => {
  let usersController: UsersController;
  beforeAll(async () => {
    usersController = global.moduleRef.get(UsersController);
  });

  afterEach(async () => {
    await getConnection().getRepository('user').delete({});
  });

  test('create', async () => {
    const a: any = await usersController.create({
      birthDate: new Date(),
      gender: UserGenderEnum.FEMALE,
      name: '123',
      password: '123',
      phone: ' 123',
    });

    expect(a.id).toEqual(1);
  });
});
