import { UserGenderEnum } from 'src/@common/enums';
import { getConnection } from 'typeorm';
import { UsersController } from '../interface/users.controller';

describe('USER: update', () => {
  let usersController: UsersController;
  beforeAll(async () => {
    usersController = global.moduleRef.get(UsersController);
  });

  let testUser: any;
  beforeEach(async () => {
    testUser = await usersController.create({
      birthDate: new Date(),
      gender: UserGenderEnum.FEMALE,
      name: '123',
      password: '123',
      phone: '1231232',
    });
  });

  afterEach(async () => {
    await getConnection().getRepository('user').delete({});
  });

  test('update', async () => {
    await usersController.update(testUser.id, {
      name: '456',
      phone: ' 123',
    });
  });
});
