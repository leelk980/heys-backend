import { BaseEvent } from 'src/@common/bases';
import { UserAggregate } from '../index';

export class UserUpdatedEvent extends BaseEvent<UserAggregate['update']> {}
