import { EventsHandler } from '@nestjs/cqrs';
import { BaseEventHandler } from 'src/@common/bases';
import { UserCreatedEvent } from './index';

@EventsHandler(UserCreatedEvent)
export class UserCreatedHandler extends BaseEventHandler<UserCreatedEvent> {
  handle(event: UserCreatedEvent) {
    // logic
    console.log(event.data, 'create user event');
  }
}
