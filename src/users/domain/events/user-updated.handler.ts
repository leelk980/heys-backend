import { EventsHandler } from '@nestjs/cqrs';
import { BaseEventHandler } from 'src/@common/bases';
import { UserUpdatedEvent } from './index';

@EventsHandler(UserUpdatedEvent)
export class UserUpdatedHandler extends BaseEventHandler<UserUpdatedEvent> {
  handle(event: UserUpdatedEvent) {
    // logic
    console.log(event.data, 'update user event');
  }
}
