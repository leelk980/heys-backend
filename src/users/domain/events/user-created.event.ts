import { BaseEvent } from 'src/@common/bases';
import { UserAggregate } from '../index';

export class UserCreatedEvent extends BaseEvent<UserAggregate['create']> {}
