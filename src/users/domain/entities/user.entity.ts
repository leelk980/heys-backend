import { BaseEntity } from 'src/@common/bases';
import { UserGenderEnum } from 'src/@common/enums';
import { Column, Entity } from 'typeorm';
import { InterestingField } from './interesting-field.entity';

@Entity()
export class User extends BaseEntity {
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  phoneNumber: string;

  @Column({ type: 'enum', enum: UserGenderEnum })
  gender: UserGenderEnum;

  @Column({ type: 'date' })
  birthDate: Date;

  @Column({ type: 'varchar' })
  password: string;

  interestingFieldList: InterestingField[];
}
