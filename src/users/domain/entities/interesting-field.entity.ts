import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../@common/bases';
import { User } from './user.entity';

@Entity()
export class InterestingField extends BaseEntity {
  @Column({ type: 'varchar' })
  name: string;

  @ManyToOne(() => User, (user) => user.id)
  user: User;
}
