import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as Jwt from 'jsonwebtoken';
import { BaseAggregate } from 'src/@common/bases';
import { EmitEvent } from 'src/@common/decorators';
import { CreateInput, UpdateInput } from 'src/@common/utils';
import { UserCreatedEvent, UserUpdatedEvent } from './events';
import { User } from './entities';
import { UserRepository } from '../infrastructure/repositories';

@Injectable()
export class UserAggregate extends BaseAggregate<User> {
  public constructor(private readonly userRepository: UserRepository) {
    super(userRepository);
  }

  /** static: root가 필요없는거*/
  @EmitEvent(UserCreatedEvent)
  async create(data: CreateInput<User, 'name'>) {
    const user = await this.userRepository.save(this.userRepository.create(data));
    return Jwt.sign({ ...user, password: undefined }, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_EXPIRES_IN,
    });
  }

  /** aggregate: root가 필요한거 */
  @EmitEvent(UserUpdatedEvent)
  async update(data: UpdateInput<User>) {
    await this.userRepository.save({ id: this.root.id, ...data });
  }

  async signIn(data: Pick<User, 'phoneNumber' | 'password'>) {
    const { phoneNumber, password } = data;
    const user = await this.userRepository.findOne({ where: { phoneNumber } });
    // const isValidPassword = checkPassword(user.password, password); // TODO
    if (!user || user.password !== password) {
      throw new HttpException('invalid_phone_or_password', HttpStatus.BAD_REQUEST);
    }

    return Jwt.sign({ ...user, password: undefined }, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_EXPIRES_IN,
    });
  }

  async changePassword(data: Pick<User, 'id' | 'password'>) {
    const { id, password } = data;
    await this.userRepository.update({ id }, { password });
  }
}
