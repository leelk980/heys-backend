import { Repository } from 'typeorm';
import { InterestingField } from '../entities';

export abstract class BaseInterestingFieldRepository extends Repository<InterestingField> {}
