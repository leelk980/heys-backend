import { Repository } from 'typeorm';
import { User } from '../entities';

export abstract class BaseUserRepository extends Repository<User> {
  public abstract findAllByUserName(): Promise<User>;
}
