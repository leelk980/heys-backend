import { ApiProperty } from '@nestjs/swagger';

export class SignInDto {
  @ApiProperty({ required: true, example: '811012345678' })
  phoneNumber: string;

  @ApiProperty({ required: true, example: 'password' })
  password: string;
}
