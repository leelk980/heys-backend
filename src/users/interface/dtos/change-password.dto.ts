import { ApiProperty } from '@nestjs/swagger';

export class ChangePasswordDto {
  @ApiProperty({ required: true, example: 'password' })
  password: string;
}
