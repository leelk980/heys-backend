import { UserGenderEnum } from 'src/@common/enums';
import { InterestingField } from '../../domain/entities';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({ required: true, example: '홍길동' })
  name: string;

  @ApiProperty({ required: true, example: '811012345678' })
  phoneNumber: string;

  @ApiProperty({ required: true, description: 'male or female', example: 'male' })
  gender: UserGenderEnum;

  @ApiProperty({ required: true, example: '2000-01-01' })
  birthDate: Date;

  @ApiProperty({ required: true, example: 'password' })
  password: string;

  @ApiProperty({ required: true })
  interestingFieldList: InterestingField[];
}
