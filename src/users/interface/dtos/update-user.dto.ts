import { UserGenderEnum } from 'src/@common/enums';

export class UpdateUserDto {
  name?: string;

  phone?: string;

  gender?: UserGenderEnum;

  birthDate?: Date;

  password?: string;
}
