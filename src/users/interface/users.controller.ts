import { Body, Controller, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiBearerAuth, ApiCreatedResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { CurrUser, Public } from 'src/@common/guards';
import { ChangePasswordDto, CreateUserDto, SignInDto, UpdateUserDto } from './dtos';
import { ChangePasswordCommand, CreateUserCommand, SignInCommand, UpdateUserCommand } from '../application/commands';
import {
  CheckUserWithPhoneNumberView,
  CreateUserView,
  FindAllInterestingFieldView,
  FindAllUserView,
  FindOneUserView,
  SignInView,
  UpdateUserView,
} from './views';
import {
  FindAllInterestingFieldQuery,
  FindAllUserQuery,
  FindOneUserQuery,
  FindUserWithPhoneNumberQuery,
} from '../application/queries';

@ApiTags('apis')
@Controller('')
export class UsersController {
  public constructor(private readonly commandBus: CommandBus, private readonly queryBus: QueryBus) {}

  @Post('/users/sign-in')
  @ApiOperation({ summary: 'sign in' })
  @ApiCreatedResponse({ status: 200, description: '200 ok', type: SignInView })
  @Public()
  async signIn(@Body() body: SignInDto): Promise<SignInView> {
    const signInCommand = new SignInCommand(body);
    return this.commandBus.execute(signInCommand);
  }

  @Get('/users/:phone_number/check')
  @ApiOperation({ summary: 'is there a user with a specific mobile phone number?' })
  @ApiParam({ name: 'phone_number', required: true, description: 'ex: 821012345678', schema: { type: 'string' } })
  @ApiCreatedResponse({ status: 200, description: 'true or false', type: CheckUserWithPhoneNumberView })
  @Public()
  async checkWithPhoneNumber(@Param('phone_number') phoneNumber: string): Promise<CheckUserWithPhoneNumberView> {
    const findUserWithPhoneNumberQuery = new FindUserWithPhoneNumberQuery(phoneNumber);
    return this.queryBus.execute(findUserWithPhoneNumberQuery);
  }

  @Get('/keywords/public')
  @ApiOperation({ summary: 'get keyword list' })
  @ApiCreatedResponse({ status: 200, description: 'true or false', type: FindAllInterestingFieldView })
  @Public()
  async getInterestingFieldList(): Promise<FindAllInterestingFieldView> {
    const findAllInterestingFieldQuery = new FindAllInterestingFieldQuery(1 as any);
    return this.queryBus.execute(findAllInterestingFieldQuery);
  }

  @Post('/users')
  @ApiOperation({ summary: 'sign up' })
  @ApiCreatedResponse({ status: 200, description: 'access token', type: CreateUserView })
  @Public()
  async signUp(@Body() body: CreateUserDto): Promise<CreateUserView> {
    const createUserCommand = new CreateUserCommand(body);
    return this.commandBus.execute(createUserCommand);
  }

  @Patch('/users/password')
  @ApiOperation({ summary: 'change password' })
  @ApiCreatedResponse({ status: 200, description: 'no response' })
  @ApiBearerAuth()
  async changePassword(@CurrUser('id') userId: number, @Body() body: ChangePasswordDto) {
    const changePasswordCommand = new ChangePasswordCommand(userId, body);
    return this.commandBus.execute(changePasswordCommand);
  }

  @Get()
  @ApiOperation({ summary: 'not used' })
  async findAll(@Query() query: string): Promise<FindAllUserView> {
    const findAllUserQuery = new FindAllUserQuery(query);
    return this.queryBus.execute(findAllUserQuery);
  }

  @Get('/:id')
  @ApiOperation({ summary: 'not used' })
  async findOne(@Param('id') id: string): Promise<FindOneUserView> {
    const findOneUserQuery = new FindOneUserQuery(+id);
    return this.queryBus.execute(findOneUserQuery);
  }

  @Patch('/:id')
  @ApiOperation({ summary: 'not used' })
  async update(@Param('id') id: string, @Body() body: UpdateUserDto): Promise<UpdateUserView> {
    const updateUserCommand = new UpdateUserCommand(+id, body);
    return this.commandBus.execute(updateUserCommand);
  }
}
