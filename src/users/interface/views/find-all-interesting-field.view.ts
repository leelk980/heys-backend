export class FindAllInterestingFieldView {
  constructor(public readonly keywordList: InterestingFieldView[]) {}
}

export class InterestingFieldView {
  constructor(public readonly id: number, public readonly name: string) {}
}
