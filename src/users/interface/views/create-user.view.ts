import { ApiProperty } from '@nestjs/swagger';

export class CreateUserView {
  @ApiProperty({ required: true, example: 'access token' })
  accessToken: string;
}
