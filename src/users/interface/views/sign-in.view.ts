import { ApiProperty } from '@nestjs/swagger';

export class SignInView {
  @ApiProperty({ required: true, example: 'access token' })
  accessToken: string;
}
