import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as CommandHandlers from './application/commands';
import * as QueryHandlers from './application/queries';
import * as EventHandlers from './domain/events';
import { UserAggregate } from './domain';
import * as Repositories from './infrastructure/repositories';
import { UsersController } from './interface/users.controller';

/** presentation */
const controllers = [UsersController];

/**  application */
const commandHandlers = Object.values(CommandHandlers);
const queryHandlers = Object.values(QueryHandlers);

/** domain */
const aggregates = [UserAggregate];
const eventHandlers = Object.values(EventHandlers);

/** infrastructure */
const repositories = Object.values(Repositories);

@Module({
  imports: [CqrsModule, TypeOrmModule.forFeature([...repositories])],
  controllers: [...controllers],
  providers: [...commandHandlers, ...queryHandlers, ...aggregates, ...eventHandlers],
})
export class UsersModule {}
