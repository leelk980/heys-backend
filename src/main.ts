import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Heys')
    .setDescription('The Heys API description')
    .setVersion('1.0')
    .addTag('heys')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  const port = +process.env.PORT;
  await app.listen(port, () => console.log(`Server is Running on ${port}`));
}
bootstrap();
