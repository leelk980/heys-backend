import { Test } from '@nestjs/testing';
import { getConnection } from 'typeorm';
import { AppModule } from './app.module';

beforeAll(async () => {
  const moduleRef = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  await moduleRef.createNestApplication().init();
  global.moduleRef = moduleRef;
});

afterAll(async () => {
  await getConnection().dropDatabase();
});
