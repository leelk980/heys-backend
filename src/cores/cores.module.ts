import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CoresController } from './interface/cores.controller';

@Module({
  imports: [CqrsModule],
  controllers: [CoresController],
})
export class CoresModule {}
