#UX WIKI
https://www.figma.com/file/3NQZ31h6qHLHjb2qifbWc3?embed_host=notion&kind=&node-id=46%3A315&viewer=1

# HOW TO START
### 1. install postgres
### 2. create 'testuser' user and 'heys_db' DB on database.
```
psql postgres 
CREATE USER testuser SUPERUSER;
CREATE DATABASE heys_db WITH OWNER testuser;
\du
\q
```
### 3. npm install & start app
```
npm install 
npm run start:dev
```
# test server SWAGGER url
```
http://ec2-54-185-82-6.us-west-2.compute.amazonaws.com:4000/api
```


Nestjs CQRS 아키텍처 적용 [nest/cqrs](https://docs.nestjs.com/recipes/cqrs).

레이어 계층구조  
![Tux, the Linux mascot](/layer-img.png)

# 레이어의 역할

### 1. Interface

- 외부로부터 dto를 받는다
- dto를 이용해 vo를 만든다(command or query)
- vo를 cqrs버스를 통해 application layer(핸들러)에 전달한다
- 외부에 view를 전달 (view는 response-dto)

### 2. Application

- vo를 정의하며 vo는 readonly인 객체이다 (command or query)
- vo의 타입은 dto를 사용한다

```typescript
// 예시
export class CreateUserCommand extends BaseCommand<CreateUserView> {
  constructor(public readonly data: CreateUserDto) {
    // vo는 dto 및 view에 의존
    super();
  }
}
```

- handler는 Interface로부터 vo를 받는다
- command handler는 domain aggregate를 호출하며 다른 도메인간의 트랜잭션을 관리한다
- query handler는 domain을 건너뛰고 repository를 직접 호출한다

### Domain

- aggregate는 도메인에 있는 entity의 집합이며, 커맨드 관련 비즈니스 로직을 수행한다
- aggregate의 메소드가 성공적으로 마무리 되면 event가 발생한다
- event-handler에서는 도메인과 관련 없는 작업을 수행한다(e.g. 이메일 발송)
- Domain 레이어는 type은 따로 정의하지 않고 entity를 이용한다

```typescript
// 예시
@EmitEvent(UserCreatedEvent)
async create(data: CreateInput<User>) {
  const user = await this.userRepository.create(data);
  console.log('create-user', user);
  return user;
}
```

### Infrastructure

- 외부와 직접적으로 통신하는 레이어이다 e.g. DB, AWS 등
- Database에 접근하는 repository와 DB 테이블은 표현하는 entity가 존재한다

### 기타) 타입에 관해서

- 레이어는 독립적이므로 각 레이어는 각자의 input과 return의 타입을 가지고 있어야한다
  - 그렇지만 현실적으로 중복되는 코드가 많을 것 같아보인다
- 절충안
  - 타입에 관해서는 (Interface & Application) + (Domain & Infra)로 구분한다
  - Interface에서 만든 dto, view 타입을 application의 vo가 의존한다
  - Infra에서 만든 entity 타입을 domain의 aggregate가 의존한다
  - Application과 Domain 사이는 dependency가 끊긴 상태
